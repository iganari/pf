ARG  _TAG=1.11.3-alpine3.8
FROM golang:${_TAG}
# https://hub.docker.com/_/golang/

RUN apk add git gcc


# RUN mkdir $HOME/src && \
#     cd $HOME/src && \
#     git clone https://github.com/gohugoio/hugo.git && \
#     cd hugo 
#     # cd hugo && \
#     # go install


ARG _VER_HUGO=0.52
ARG _VER_BIN=hugo_${_VER_HUGO}_Linux-64bit

RUN mkdir /usr/local/src
ADD https://github.com/gohugoio/hugo/releases/download/v${_VER_HUGO}/${_VER_BIN}.tar.gz /usr/local/src/
RUN tar xzf /usr/local/src/${_VER_BIN}.tar.gz -C /usr/local/src && \
    ln -s /usr/local/src/hugo /usr/local/bin/hugo && \
    rm -f /usr/local/src/${_VER_BIN}.tar.gz

EXPOSE 1313
CMD hugo version
