# pf

## 用途

+ 自分のポートフォリサイト
+ ここから各種のサービスにリンクしていく感じしたい
+ このサイトさえ見せれば、自分のすべてのアウトプットに繋がってくれれば良い

## 仕様

+ HUGO
    + Golang製の静的サイトジェネレーター
    + https://gohugo.io/
    + 使用しているテンプレート
        + [naro143/hugo-coder-portfolio](https://github.com/naro143/hugo-coder-portfolio) 

## 開発環境構築


+ themesをgitクローンしておきます

```
git clone https://github.com/naro143/hugo-coder-portfolio.git themes/hugo-coder-portfolio
```

+ Dockerコンテナを起動します ---> :whale:

```
sh docker-build-run.sh run
```

+ :whale: hugoserver起動方法
    + Draftの記事も表示させる -> `--buildDrafts`

```
hugo server --bind="0.0.0.0" --buildDrafts
```

+ ブラウザで確認する

http://127.0.0.1:1313/pf

