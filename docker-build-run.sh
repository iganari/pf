#!/bin/bash

set -xeu

_I_TAG='portfolio'
BASEPATH=$(cd `dirname $0`; pwd)
DIREPATH=`echo $BASEPATH | awk -F\/ '{print $NF}'`

prepare()
{
  set +eu
  docker rm -f ${_I_TAG}
  set -eu
  
  docker build . -t ${_I_TAG}
}

build()
{
  docker run -d \
             -v $BASEPATH:/opt/hejda/$DIREPATH \
             -w /opt/hejda/$DIREPATH/mysite-nix \
             -h ${_I_TAG} \
             -p 1313:1313 \
             --name ${_I_TAG} \
             ${_I_TAG} \
             hugo server --bind="0.0.0.0"
}
run()
{
  docker run --rm \
             -it \
             -v $BASEPATH:/opt/hejda/$DIREPATH \
             -w /opt/hejda/$DIREPATH \
             -h ${_I_TAG} \
             -p 1313:1313 \
             --name ${_I_TAG} \
             ${_I_TAG} \
             /bin/sh
}

## Main

if [ "$1" = 'build' ]; then
  echo 'build'
  prepare
  build
elif [ "$1" = 'run' ]; then
  echo 'run'
  prepare
  run
else
  echo 'Arugment is {build|run} Only'
fi
